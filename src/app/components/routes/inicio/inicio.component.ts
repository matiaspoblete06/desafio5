import { Component, OnInit } from '@angular/core';
import { MoviesSeries } from 'src/interfaces/MoviesSeries';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  Category: string = "Todos";
  filter: string = "todos";
  name: any;


  movies_series: MoviesSeries[] = [
    {
      id: 1,
      name: "Black Widow",
      description: "Natasha Romanoff, alias Viuda Negra, se enfrenta a las partes más oscuras de su historia cuando surge una peligrosa conspiración con vínculos con su pasado. Perseguida por una fuerza que no se detendrá ante nada para acabar con ella, Natasha debe enfrentarse a su historia como espía y a las relaciones rotas que dejó a su paso mucho antes de convertirse en una Vengadora.",
      image: "../assets/cards-image/img1.png",
      rating: 7.5,
      category: "Pelicula"
    },
    {
      id: 2,
      name: "Shang Chi",
      description: "Adaptación cinematográfica del héroe creado por Steve Englehart y Jim Starlin en 1973, un personaje mitad chino, mitad americano, cuyo característico estilo de combate mezclaba kung-fu, nunchacos y armas de fuego.",
      image: "../assets/cards-image/img2.png",
      rating: 7.8,
      category: "Pelicula"
    },
    {
      id: 3,
      name: "Loki",
      description: "La nueva serie Loki, de Marvel Studios, empieza allí donde terminó Vengadores: Endgame. En ella, el voluble villano Loki vuelve a ganarse el apodo de Dios del Engaño.",
      image: "../assets/cards-image/img3.png",
      rating: 8.2,
      category: "Pelicula"
    },
    {
      id: 4,
      name: "How I Met Your Mother",
      description: "Natasha Romanoff, alias Viuda Negra, se enfrenta a las partes más oscuras de su historia cuando surge una peligrosa conspiración con vínculos con su pasado. Perseguida por una fuerza que no se detendrá ante nada para acabar con ella, Natasha debe enfrentarse a su historia como espía y a las relaciones rotas que dejó a su paso mucho antes de convertirse en una Vengadora.",
      image: "../assets/cards-image/img4.png",
      rating: 7.5,
      category: "Pelicula"
    },
    {
      id: 5,
      name: "Peaky Blinders",
      description: "En Gran Bretaña, Reino Unido se recuperan de la desesperación de la Gran Guerra, las personas sobreviven a como pueden, y las bandas criminales proliferan en una nación sacudida económicamente.",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/dYAeYkRqDfQrMGHdY7hrtllSPTu.jpg",
      rating: 8.6,
      category: "Serie"
    },
    {
      id: 6,
      name: "Grey Anatomy ",
      description: "La vida de Meredith Grey no es nada fácil. Intenta tomar las riendas de su vida, aunque su trabajo sea de esos que te hacen la vida imposible. Meredith es una cirujana interna de primer año en el Hospital Grace de Seattle, el programa de prácticas más duro de la Facultad de Medicina de Harvard. Y ella lo va a comprobar..",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/9ii38fa8Vqb9dRv97qEvoGbGftE.jpg",
      rating: 8.3,
      category: "Serie"
    },
    {
      id: 7,
      name: "The Flash",
      description: "Después de que un acelerador de partículas cause una extraña tormenta, al investigador científico de la policía, Barry Allen, le cae un rayo y entra en coma. Meses después despierta con el poder de moverse a súper velocidad permitiéndole ser el ángel de la guardia de Central City..",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/2O8tlUzKS3uddGW5D2B9XM6b4e.jpg",
      rating: 7.8,
      category: "Serie"
    },
    {
      id: 8,
      name: "Anne with an E",
      description: "Anne Shirley es una niña huérfana que vive en un pequeño pueblo llamado Avonlea que pertenece a la Isla del Príncipe Eduardo, en el año 1890. Después de una infancia difícil, donde fue pasando de orfanato a hogares de acogida, es enviada por error a vivir con una solterona y su hermano. Cuando cumple 13 años, Anne va a conseguir transformar su vida y el pequeño pueblo donde vive gracias a su fuerte personalidad, intelecto e imaginación..",
      image: "https://www.themoviedb.org/t/p/w600_and_h900_bestv2/6P6tXhjT5tK3qOXzxF9OMLlG7iz.jpg",
      rating: 8.8,
      category: "Serie"
    }
    ]

    
  constructor() { }

  ngOnInit(): void {
    this.filter = 'Todos'
  }
  clickTodos() {
    this.filter = 'Todos'
  }
  clickPeliculas () {
    this.filter = 'Pelicula';
    this.Category = 'Pelicula';
  }
  
  clickSeries () {
    this.filter = 'Serie';
    this.Category = 'Serie';
  }

  Search() {
    if(this.name == "") {
      this.ngOnInit();
    }else{
      this.movies_series = this.movies_series.filter( res => {
        return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
      });
    }
          }
  }
